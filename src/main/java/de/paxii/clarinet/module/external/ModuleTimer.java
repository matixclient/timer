package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.module.settings.ValueBase;

import net.minecraft.util.Timer;

/**
 * Created by Lars on 28.07.17.
 */
public class ModuleTimer extends Module {

  public ModuleTimer() {
    super("Timer", ModuleCategory.MOVEMENT);

    this.setVersion("1.0.1");
    this.setBuildVersion(18201);
    this.setDescription("Changes the speed of everything.");

    this.getModuleValues().put("speed", new ValueBase("Speed", 2.0F, 0.5F, 10.0F) {
      @Override
      public void onUpdate(float oldValue, float newValue) {
        ModuleTimer.this.setTimer(newValue);
      }
    });
  }

  @Override
  public void onEnable() {
    this.setTimer(this.getValueBase("speed").getValue());
  }

  private void setTimer(float speed) {
    Wrapper.getMinecraft().setTimer(new Timer(speed * 20.0F));
  }

  @Override
  public void onDisable() {
    this.setTimer(1.0F);
  }

}